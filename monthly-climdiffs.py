''' Relative Climatology Differences
    ================================
    Consider a single simulation with the idea to pick individual simulations 
    apart first, rather than naïvely loop over a bunch of simulations and then 
    work backwards for explanations when our results don't match intuitions.

    Output is a panel plot of relative differences between monthly 
    climatologies. 
                                                                            '''
import matplotlib.pyplot as plt             
import matplotlib as mpl
import seaborn as sns
import xarray as xr
import pandas as pd
import numpy as np
import cartopy
import sys
import glob as grod
import cftime

from cartopy.io.img_tiles import Stamen
from cdo import *
cdo = Cdo()

metprm = sys.argv[1]  #'pr'

print ('hello')

''' Along with libraries above will pull in some possible fancy map 
    backgrounds (each time draw a map the program makes and online quiery to 
    pull in the image, so can be a bit slow).  
                                                                            '''
stamen_terrain = Stamen('terrain-background') 
stamen_toner   = Stamen('toner-background')

def get_year(x):
    ''' 
    Regonize the type of datetime in different simulations and 
    give the correct scripts to read the year in simulations
    '''
    if type(x) == cftime._cftime.DatetimeNoLeap:
        return x.year
    elif type(x) == cftime._cftime.Datetime360Day:
        return x.year		
    else:
        return pd.to_datetime(x).year

def set_values(hsim_path,psim_path,metprm) :
    ''' 
    Read in files and calculate tables of relative differences that can 
    be passed to plotting routines. 

    Need to pick years out of what we have access to that we want to make 
    a climatology from. Should use the same number of years in both for 
    consistancy (if not need to be very careful about how significance of 
    change is computed to account for the uncertainy in the means being 
    different). 
    * Here we have hard coded the periods between 1976:2005 and 
      2021:2050. That should be adjusted if desired. 


    Variables
    ---------
    hsim_path : path to historical simulation (str)
    psim_path : path to projection simulation (str)
    metprm : name of meteorological parameter being investigated (str)
    '''

    ## Read in data
    H = xr.open_dataset(hsim_path)
    P = xr.open_dataset(psim_path)

    ## Change units
    if metprm == 'pr' : 
        H.pr.values = 86400*H.pr.values
        P.pr.values = 86400*P.pr.values
        H.pr.attrs['units'] = 'mm/month'
        P.pr.attrs['units'] = 'mm/month'

    ## Monthly Climatologies
    idx_clim = [get_year(x) 
                in range(1976,2006) 
                for x in H.time.values[:]]
    H = H.sel(time=idx_clim)

    idx_clim = [get_year(x)
                in range(2021,2051) 
                for x in P.time.values[:]]
    P = P.sel(time=idx_clim)

    nyrs = len(
        np.unique(
            [get_year(x) for x in H.time.values]
        ))

    H_clim = H.groupby('time.month').mean('time')
    H_clim['StndDev'] = H.groupby('time.month').std('time')[metprm]
    H_clim['StndErr'] = H_clim['StndDev']/np.sqrt(nyrs)

    nyrs = len(
        np.unique(
            [get_year(x) for x in P.time.values]
        ))

    P_clim = P.groupby('time.month').mean('time')
    P_clim['StndDev'] = P.groupby('time.month').std('time')[metprm]
    P_clim['StndErr'] = P_clim['StndDev']/np.sqrt(nyrs)


    ''' 
    Now calculate the relative difference between the climatology means in 
    the projection and the 'base line' values. As well, can can calculate 
    how many sigma levels we need to exapand the standard errors of those 
    means in order to get the raw differences to overlap, which here 
    functions as an addhoc probability of the differences being significant. 
    * Would be better to estimate this through bootstraping, and maybe to 
      do the same with the standard deviation values as well.
    '''


    D = H_clim.copy(deep=True)                   # create a new copy of grid
    D = D.drop([metprm,'StndDev','StndErr'])     # make it an empty copy

    D['raw_diff'] = (                            # diff climatologies
        P_clim[metprm] - H_clim[metprm] )
    
    D['rel_diff'] = D.raw_diff/H_clim[metprm]    # diff in mean
                                                 # ... as % of historical
    
    D['dev_diff'] = (                            # diff in variation
        P_clim.StndDev - H_clim.StndDev )        # ... as % of historical
    D['rel_dev'] = D.dev_diff/H_clim.StndDev

    ''' Record how many stnd err devs between the climatology means '''
    D['sig_diff'] = D.raw_diff.copy(deep=True)   
    D['sig_diff'].values = (D.sig_diff.values*0)+4  
    for m in range(1,13) : 
        for sig_lev in range(4,0,-1) : 
            D['sig_diff'].sel(month=m).values[
                np.abs(D.raw_diff.sel(month=m)) 
                <= (sig_lev*P_clim.StndErr.sel(month=m)
                    + sig_lev*H_clim.StndErr.sel(month=m))] = sig_lev
        

    return(D)


def show_range(D,latlons) :
    '''
    Write out the range of values in the difference statistics

    Variables
    ---------
    D : data file containing difference statistics  (xarray)
    Return a matrix (array) with all difference statistics data
    '''
    if latlons:
        min_lat =  latlons[0] #D.lat.values.min()
        max_lat =  latlons[1] #D.lat.values.max()
        min_lon =  latlons[2] #D.lon.values.min()
        max_lon =  latlons[3] #D.lon.values.max()
        min_mean_diff=np.round(D.rel_diff.where((D.lat>min_lat)
				& (D.lat<max_lat)&(D.lon<max_lon)&(D.lon>min_lon),
				drop=True).min().values,decimals=2)*100
        max_mean_diff=np.round(D.rel_diff.where((D.lat>min_lat)
				&(D.lat<max_lat)&(D.lon<max_lon)&(D.lon>min_lon),
				drop=True).max().values,decimals=2)*100
        min_stnd_dev=np.round(D.rel_dev.where((D.lat>min_lat)&
				(D.lat<max_lat)&(D.lon<max_lon)&(D.lon>min_lon)
				,drop=True).min().values,decimals=2)*100
        max_stnd_dev=np.round(D.rel_dev.where((D.lat>min_lat)&
				(D.lat<max_lat)&(D.lon<max_lon)&(D.lon>min_lon),
				drop=True).max().values,decimals=2)*100
        mean_mean_diff=np.round(D.rel_diff.where((D.lat>min_lat)
				&(D.lat<max_lat)&(D.lon<max_lon)&(D.lon>min_lon),
				drop=True).mean().values,decimals=2)*100
    else:
        min_mean_diff=np.round(D.rel_diff.min().values,decimals=2)*100
        max_mean_diff=np.round(D.rel_diff.max().values,decimals=2)*100
        min_stnd_dev=np.round(D.rel_dev.min().values,decimals=2)*100
        max_stnd_dev=np.round(D.rel_dev.max().values,decimals=2)*100
        mean_mean_diff=np.round(D.rel_diff.mean().values,decimals=2)*100
    print('---------------------------------------')
    print('min mean diff (as %): ',
          str(min_mean_diff),'%')
    print('max mean diff (as %): ',
          str(max_mean_diff),'%')
    print('mean mean diff (as %): ',
          str(mean_mean_diff),'%')
    print()
    print('min stnd.dev. diff (as %): ',
          str(min_stnd_dev),'%')
    print('max stnd.dev. diff (as %): ',
          str(max_stnd_dev),'%')
    print('---------------------------------------')
    
    #matrix=np.append(matrix,[[min_mean_diff,max_mean_diff,min_stnd_dev,max_stnd_dev]],axis=0) #
    #return (matrix)
    return (min_mean_diff,max_mean_diff,min_stnd_dev,max_stnd_dev)

def map_values(D,fname_hsim,latlons) :
    '''
    Draw values on a map.

    Variables
    ---------
    D : data file containing statistics to be plot (xarray)
    '''
    if metprm == 'pr' :
    	color_map = 'PuOr'
    	variable_name = 'Precipitation'
    	scatter_name = ' Percent Change (%)'
    	contour_name = ' Standard Deviation Change (%)'
    elif metprm =='tasmax':
    	color_map = 'RdYlBu_r'
    	variable_name = 'Maximum Temperature'
    	contour_name = ' Standard Deviation Change (%)'
    else :
    	color_map = 'RdYlBu_r' 
    	variable_name = 'Max Wind Speed' 
    	contour_name = ' Standard Deviation Change (%)'

    min_lat =  latlons[0] #D.lat.values.min()
    max_lat =  latlons[1] #D.lat.values.max()
    min_lon =  latlons[2] #D.lon.values.min()
    max_lon =  latlons[3] #D.lon.values.max()
    clon = min_lon + (max_lon - min_lon)/2.0
    clat = min_lat + (max_lat - min_lat)/2.0 
    month_lab=['January','February', 'March','April',
               'May','June','July','August',
               'September','October','November','December']
    
    month = range(1,13);

    fig,axs = plt.subplots(            # add 12 subplots and remove axs
        frameon=False, nrows=3,
        ncols=4,figsize=(28, 18),
        subplot_kw={'xticks': [], 'yticks': []}
    ) 

    fig.subplots_adjust(               # adjust subplots positions and spaces
        left=0.01, right=0.90,
        top =0.95, bottom=0.05,
        hspace=0.06, wspace=0.05) 

    for i in range (0,3):              # deleted the edge line of subpolts
        for j in range (0,4):
            axs[i,j].spines['left'].set_visible(False)
            axs[i,j].spines['right'].set_visible(False)
            axs[i,j].spines['bottom'].set_visible(False)
            axs[i,j].spines['top'].set_visible(False)

    for i in range(0,12):
        chart = fig.add_subplot(                       # add projection map
            3, 4, i+1,projection=cartopy.crs.LambertConformal(
                central_longitude=clon,central_latitude=clat))

        _ = chart.set_extent(
            [min_lon, max_lon, min_lat, max_lat], 
            crs=cartopy.crs.PlateCarree())

        _ = chart.add_image(stamen_terrain,8,interpolation='spline16')

        m0 = chart.scatter(                  # add scatter plot with relative difference (color) and significant SD level (size) 
            D.lon,D.lat,
            transform=cartopy.crs.PlateCarree(),
            cmap=color_map,    
            c=D.rel_diff.sel(month=month[i])*100,
            s=D.sig_diff.sel(month=month[i])*5,    
            vmin=-40,vmax=40)   

        rd = D.rel_dev.sel(month=month[i])*100   # add contour lines for SD difference
        
        #create a reverse colorbar for [-90, 0] from grey to white
        bounds=np.arange(-90,30,30)	
        cmap=plt.get_cmap('Greys_r',len(bounds))
	      
        m1 = rd.plot.contour(
            'lon','lat',ax=chart,
            levels=np.arange(-90,30,30),
            linewidths=4,linestyles='dashed',
            transform=cartopy.crs.PlateCarree(),
            add_colorbar=False,
            vmin=-40,vmax=0,
            alpha=0.9,cmap=cmap)

        m2 = rd.plot.contour(
            'lon','lat',ax=chart,
            levels=np.arange(0,120,30),
            linewidths=4,linestyles='solid',
            transform=cartopy.crs.PlateCarree(),
            add_colorbar=False,
            vmin=0,vmax=40,
            alpha=0.9,cmap='Greys')

        _ = plt.title(month_lab[i],fontsize=14)


    fig.suptitle('_'.join(fname_hsim.split('_')[1:5])
               		+'_'+'_'.join(fname_hsim.split('_')[6:9])
			, fontsize=16)
    plt.colorbar(
        m0, cax = plt.axes([0.91, 0.05, 0.015, 0.9]),
        label=variable_name+scatter_name)
    #add two contour lines colorbar
    cmap=plt.get_cmap('Greys_r',len(bounds))
    norm=mpl.colors.BoundaryNorm(bounds,len(bounds))
    ax_coutour=fig.add_axes([0.96, 0.05, 0.015, 0.45])
    mpl.colorbar.ColorbarBase(ax_coutour,cmap = cmap,
	norm=norm)
    bounds=np.arange(0,120,30)
    cmap=plt.get_cmap('Greys',len(bounds))
    norm=mpl.colors.BoundaryNorm(bounds,len(bounds))
    ax_coutour=fig.add_axes([0.96, 0.5, 0.015, 0.45])
    mpl.colorbar.ColorbarBase(ax_coutour,cmap = cmap,
	norm=norm)
    #add text on figures to explain contour lines
    plt.figtext(0.99,0.6,variable_name+contour_name,fontsize=10,rotation='vertical')
    explain='----- Changes of Standard deviation < 0, ───── Changes of Standard deviation > 0'
    plt.figtext(0.35,0.03,explain,fontsize=14,rotation='horizontal')

    plt.savefig('_'.join(fname_hsim.split('_')[1:5])
               		+'_'+'_'.join(fname_hsim.split('_')[6:9])
			+'.png')
    plt.close()

    
# ________________________________________________________________________
# ::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::



''' Pull data
    ---------
    Data paths and file names will vary. 
                                                                            '''
datapath = '/home/ubuntu/eucordex/NorthSea'
#datapath = '/home/zheng'
''' Different met-parameters will have different associated aggregations. 
    * May want to edit so that aggregation is set based on a seperate 
      external argument, since in future we might have multiple aggregations
      performed on the same parameters.

                                                               '''
fname_hsim=[]
fname_psim=[]
matrix_channel=[[0,0,0,0]]
matrix_stats=[[0,0,0,0]] #make an empty matrix

if metprm == 'pr' :
    aggr = 'monsum_' 
elif metprm =='tasmax':
    aggr = 'monmax_'
else :
    aggr = ''

#grab different simulations 
fname_hsim += grod.glob(datapath+'/'+aggr+'NorthSea_'+metprm+'*'           
              +'historical'+'*.nc')
fname_psim += grod.glob(datapath+'/'+aggr+'NorthSea_'+metprm+'*'
              +'rcp85'+'*.nc')

# make same consequence for historical and rcp85 files
fname_hsim.sort()                   
fname_psim.sort()

# loop to process simulations one by one
print(fname_hsim)
print(fname_psim)

# if there is a range selected:
graph_range=[50.0,54.0,-2.0,6.0] #input consequence: min_lat,max_lat,min_lon,max_lon
 
for idx in range(0, len(fname_hsim)):                       
    print('processing ', fname_hsim[idx])
    D = set_values(fname_hsim[idx],fname_psim[idx],metprm=metprm)		
    matrix_stats=np.append(matrix_stats,[show_range(D,None)],axis=0)
    matrix_channel=np.append(matrix_channel,[show_range(D,graph_range)],axis=0)
    map_values(D,fname_psim[idx],graph_range)

'''Extract the statistics results of different range (NorthSea, The Channel)
                       '''                                                           
matrix_stats=np.delete(matrix_stats,0,axis=0)  #Delete the first fake row (0 0 0 0)
df=pd.DataFrame(columns=['min_mean_diff','max_mean_diff','min_stnd_dev','max_stnd_dev'])      # make a dataset to save columns names
for idx in range(0,len(matrix_stats)):
	df.loc[idx]=matrix_stats[idx]           # write each simulation statistics results to dataset
	
df.to_csv(r'stats_'+metprm+'.csv',index=False)       #save dataset as csv

matrix_channel=np.delete(matrix_channel,0,axis=0)  #Delete the first fake row (0 0 0 0)
df=pd.DataFrame(columns=['min_mean_diff','max_mean_diff','min_stnd_dev','max_stnd_dev'])      # make a dataset to save columns names
for idx in range(0,len(matrix_channel)):
	df.loc[idx]=matrix_channel[idx]           # write each simulation statistics results to dataset
	
df.to_csv(r'stats_channel_'+metprm+'.csv',index=False)       #save dataset as csv


# ==:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:==

