''' Prepossess EU-CORDEX
    --------------------
    Outline a script to merge and subset CORDEX files aquired 
    through provided downloading scripts.                            

    The metprms considered are assigned by arguments so use for example:
    `$ python precproc-cordex.py uas vas`

    * Assumes a certain directory structure. Edit the `datadir` variable 
      if this doesn't fit.                                                  '''

import glob as grod
import numpy as np
import sys
import os

from cdo import *
cdo = Cdo()

data_product = 'eucordex'                # choose data product
region_lab = 'NorthSea'                  # choose sub-region
metprms = sys.argv[1:]                   # use externally set fields
                                         # ... or leave empty list
                                         # ... to process whole folder

''' Set variables
    -------------
    Here set some values so that can make meta choices to describe what
    regions etc, that we want to deal with, and hold the variables these
    imply in the following tables/variables. 
    * Set a dictionary that matches a region label to a bounding box.
      Box is described as a string with form that matches syntax for 
      the cdo command `selonlatbox`:
      `[MIN-LON],[MAX-LON],[MIN-LAT],[MAX-LAT]`                            '''

region_boxes = {'NorthSea' : '-10,12,49,62'}
product_keys = {'eucordex' : 'EUR-11'}

# ====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====

''' Find data files
    ---------------
    Go to the specified directory and list out the CORDEX files contained
    within. Do this using windcards (*) with the assumption that the files
    are in NetCDF format (.nc).                                            '''

datadir = '/home/ubuntu/'+data_product
print(datadir)
os.chdir(datadir)
filenames = []
if data_product == 'eucordex' :
    if metprms :
        for metprm in metprms :
            filenames += grod.glob(
                metprm+'*'+product_keys[data_product]+'*.nc')
    else :
        print('*'+product_keys[data_product]+'*.nc')
        grod.glob(
            '*'+product_keys[data_product]+'*.nc')
else :
    print("+++ Unknown data product +++")

filenames = np.array(filenames)

''' Process files
    -------------
    * Find 'set' of simulations, using the same models, over the same period,
      assuming the same scenario, brocken up by time intervals. 
    * Subset these spatially to the region of interest.                   
    * Merge these sperate time slices into one file.                     
    * Aggrigate as appropriate for given                                    '''

if not os.path.isdir(region_lab) :                 # create storage directory
    print(
        "* Creating storage directory: "
        +region_lab)
    os.mkdir(region_lab)
    
headers = np.array(['_'.join(x.split('_')[:-1]) for x in filenames])
for sim in np.unique(headers) :
    print("* Processing : "+sim)
    ix = (sim == headers)
    count = 0 
    for filename in filenames[ix] :
        cdo.sellonlatbox(                          # select region
            region_boxes[region_lab],input=filename,
            output='tmp'+str(count)+'.nc')
        count += 1

    subsets = grod.glob('tmp*.nc')
    cdo.mergetime(                                 # merge times
        input=' '.join(subsets),
        output=region_lab+'/'+region_lab+'_'+sim+'.nc')

    for subset in subsets :                        
        os.remove(subset)                          # tidy tmp files


    ## Aggregations 
    metprm = sim.split('_')[0]
    if metprm == 'pr' :
        cdo.monsum(
            input=region_lab+'/'+region_lab+'_'+sim+'.nc',
            output=region_lab+'/monsum_'+region_lab+'_'+sim+'.nc')
        cdo.yearmax(
            input=region_lab+'/'+region_lab+'_'+sim+'.nc',
            output=region_lab+'/yearmax_'+region_lab+'_'+sim+'.nc')
        os.remove(region_lab+'/'+region_lab+'_'+sim+'.nc')
    if metprm in ['uas','vas'] : 
        cdo.monmean(
            input=region_lab+'/'+region_lab+'_'+sim+'.nc',
            output=region_lab+'/monmean_'+region_lab+'_'+sim+'.nc')
    #    os.remove(region_lab+'/'+region_lab+'_'+sim+'.nc')
    if metprm in ['tasmax'] : 
        cdo.monmax(
            input=region_lab+'/'+region_lab+'_'+sim+'.nc',
            output=region_lab+'/monmax_'+region_lab+'_'+sim+'.nc')
        os.remove(region_lab+'/'+region_lab+'_'+sim+'.nc')
    if metprm in ['clt'] : 
        cdo.monmean(
            input=region_lab+'/'+region_lab+'_'+sim+'.nc',
            output=region_lab+'/monmean_'+region_lab+'_'+sim+'.nc')
        os.remove(region_lab+'/'+region_lab+'_'+sim+'.nc')
    if metprm in ['rsds'] : 
        cdo.monmean(
            input=region_lab+'/'+region_lab+'_'+sim+'.nc',
            output=region_lab+'/monmean_'+region_lab+'_'+sim+'.nc')
        os.remove(region_lab+'/'+region_lab+'_'+sim+'.nc')

            
# ====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====
